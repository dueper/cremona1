<?php
/**
 * Template Name: Aggiungi SMS
 * The template used for displaying page content in page.php
 *
 * @author Dueper Design
 * @package Dueper Theme
 */

$text 			= $_GET["txtsms"];
$cell_number	= $_GET["numsms"];
$time			= $_GET["datasms"]; // FORMATO: [ Y-m-d H:i:s ]

// aggiungo custom post type
add_action( 'wp_insert_post', 'aggiungo_custom_field' );
function aggiungo_custom_field( $post_id ) {
	add_post_meta( $post_id, 'nome', 'valore' );
}


if($text & $cell_number):

	$post = array(
				'post_title'	=> $cell_number,
				'post_content'	=> $text,
                'post_type'     => 'sms',
				'post_status'	=>'publish',
				'post_status'	=>'publish',
				//'post_date_gmt'  => $time,
				);
	$new_SMS_id = wp_insert_post( $post, $wp_error );
    update_post_meta( $new_SMS_id, 'da_leggere', 1 );
	
	echo '<h1>SMS aggiunto</h1>';
    echo '<strong>numero:</strong><br />'.$cell_number.'<br /><br />';
    echo '<strong>testo:</strong><br />'.$text.'<br /><br />';

else :
	if(!$text) echo 'Manca il testo dell\'SMS <br/>';
	if(!$cell_number) echo 'Manca il numero di cellulare <br/>';
endif; ?>