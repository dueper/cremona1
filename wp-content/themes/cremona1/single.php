<?php
/**
 * The template for displaying all posts.
 *
 * Default Post Template
 *
 * Page template with a fixed 940px container and right sidebar layout
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="container">
    <div class="row">
    	<div class="span12">
    		<?php if (function_exists('bootstrapwp_breadcrumbs')) bootstrapwp_breadcrumbs(); ?>
        </div><!--span12-->
    </div><!--/.row -->
</div><!--/.container -->
<div class="container">     
    <div class="row content">
        <div class="span8">
            <header class="post-title">
                <h1><?php the_title();?></h1>
            </header>
            <p class="meta"><span class="entypo small">&#128340;</span>&nbsp;<?php the_date(); ?></p>
            <?php the_post_thumbnail('large8',array('class'=>'img-polaroid green')); ?>
            
				<?php the_content();?>
                <?php the_tags( '<p>Tags: ', ', ', '</p>'); ?>
        
        <hr />
        <?php comments_template(); ?>
        
        <?php bootstrapwp_content_nav('nav-below');?>
        </div><!--span8-->
        
        <div class="span4">
              <?php get_sidebar(); ?>
        </div><!--span4-->
    </div><!-- .row content -->
</div><!--container-->
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>