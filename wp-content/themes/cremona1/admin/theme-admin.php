<?php
 /********************************************************************
 *
 * 			Creo l'area del tema nell'admin di wordpress
 *
 *********************************************************************/
 
$themename = get_current_theme();
$shortname = "nt";

$categories = get_categories('hide_empty=0&orderby=name');
$wp_cats = array();
foreach ($categories as $category_list ) {
       $wp_cats[$category_list->cat_ID] = $category_list->cat_name;
}
array_unshift($wp_cats, "Choose a category"); 

$options = array (
 
array( "name" => $themename." Options",
	"type" => "title"),
 

array( "name" => "General",
	"type" => "section"),
array( "type" => "open"),
 
array( "name" => "Debug Mode",
	"desc" => "Flaggare per attivare la debug mode",
	"id" => "dueper_debug",
	"type" => "checkbox",
	"std" => false),
	
array( "name" => "Animate.css",
	"desc" => "Flaggare per attivare animate.css",
	"id" => "dueper_animate",
	"type" => "checkbox",
	"std" => true),
	
array( "name" => "Logo URL",
	"desc" => "Enter the link to your logo image",
	"id" => "dueper_logo",
	"type" => "text",
	"std" => ""),
	
array( "name" => "Logo URL 2",
	"desc" => "Enter the link to your logo image",
	"id" => "dueper_logo2",
	"type" => "text",
	"std" => ""),
	
array( "name" => "Qualità JPEG caricati",
	"desc" => "Selezionare un valore:<br/><ul><li><strong>50/60</strong> -> Bassa qualità</li><li><strong>70/80</strong> -> Buona qualità</li><li><strong>90/100</strong> -> Ottima qualità</li></ul><br/>Ricorda che la qualità delle immagini influisce sulla velocistà di caricamento del sito web.<br/>Qualità più bassa = Sito web più veloce<br/>",
	"id" => "dueper_compressione_jpeg",
	"type" => "select",
	"options" => array("50", "60", "70","80", "90","100"),
	"std" => "80"),
	
array( "name" => "Google Analytics Code",
	"desc" => "You can paste your Google Analytics or other tracking code in this box. This will be automatically added to the footer.",
	"id" => "dueper_ga_code",
	"type" => "textarea",
	"std" => ""),	
		
array( "name" => "Custom Favicon",
	"desc" => "A favicon is a 16x16 pixel icon that represents your site; paste the URL to a .ico image that you want to use as the image",
	"id" => "dueper_favicon",
	"type" => "text",
	"std" => get_bloginfo('url') ."/favicon.ico"),	
	
array( "name" => "Image iPhone 57x57",
	"desc" => "Inserire URL dell'icona 57x57",
	"id" => "dueper_iphone_57",
	"type" => "text",
	"std" => ""),
	
array( "name" => "Image iPhone 72x72",
	"desc" => "Inserire URL dell'icona 72x72",
	"id" => "dueper_iphone_72",
	"type" => "text",
	"std" => ""),
	
array( "name" => "Image iPhone 114x114",
	"desc" => "Inserire URL dell'icona 114x114",
	"id" => "dueper_iphone_114",
	"type" => "text",
	"std" => ""),
	
array( "name" => "Image iPhone 144x144",
	"desc" => "Inserire URL dell'icona 144x144",
	"id" => "dueper_iphone_144",
	"type" => "text",
	"std" => ""),
	
array( "name" => "Custom CSS",
	"desc" => "Want to add any custom CSS code? Put in here, and the rest is taken care of. This overrides any other stylesheets. eg: a.button{color:green}",
	"id" => "dueper_custom_css",
	"type" => "textarea",
	"std" => ""),		
	
array( "type" => "close"),
array( "name" => "Social",
	"type" => "section"),
array( "type" => "open"),

array( "name" => "Facebook URL",
	"desc" => "Url of Facebook Page or Account",
	"id" => "dueper_facebook",
	"type" => "text"),

array( "name" => "Twitter URL",
	"desc" => "Url of Twitter Page",
	"id" => "dueper_twitter",
	"type" => "text"),	
	
array( "name" => "Youtube URL",
	"desc" => "Url of Youtube channel",
	"id" => "dueper_youtube",
	"type" => "text"),	

	
array( "type" => "close"),
array( "name" => "Homepage",
	"type" => "section"),
array( "type" => "open"),

array( "name" => "Homepage header image",
	"desc" => "Enter the link to an image used for the homepage header.",
	"id" => "dueper_header_img",
	"type" => "text",
	"std" => ""),
	
array( "name" => "Homepage featured category",
	"desc" => "Choose a category from which featured posts are drawn",
	"id" => "dueper_feat_cat",
	"type" => "select",
	"options" => $wp_cats,
	"std" => "Choose a category"),
	
array( "name" => "Includi Blocco 1",
	"desc" => "Flaggare per inserire il blocco in homepage",
	"id" => "dueper_blocco1",
	"type" => "checkbox",
	"std" => true),
	

array( "type" => "close"),
array( "name" => "Footer",
	"type" => "section"),
array( "type" => "open"),
	
array( "name" => "Footer copyright text",
	"desc" => "Enter text used in the right side of the footer. It can be HTML",
	"id" => "dueper_footer_text",
	"type" => "text",
	"std" => ""),


 
array( "type" => "close")
 
);


function mytheme_add_admin() {
 
global $themename, $shortname, $options;
 
if ( $_GET['page'] == basename(__FILE__) ) {
 
	if ( 'save' == $_REQUEST['action'] ) {
 
		foreach ($options as $value) {
		update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }
 
foreach ($options as $value) {
	if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }
 
	header("Location: admin.php?page=theme-admin.php&saved=true");
die;
 
} 
else if( 'reset' == $_REQUEST['action'] ) {
 
	foreach ($options as $value) {
		delete_option( $value['id'] ); }
 
	header("Location: admin.php?page=theme-admin.php&reset=true");
die;
 
}
}
 
add_menu_page($themename, $themename, 'administrator', basename(__FILE__), 'mytheme_admin');
}

function mytheme_add_init() {

$file_dir=get_bloginfo('template_directory');
wp_enqueue_style("admin", $file_dir."/admin/admin.css", false, "1.0", "all");
wp_enqueue_script("rm_script", $file_dir."/admin/rm_script.js", false, "1.0");

}
function mytheme_admin() {
 
global $themename, $shortname, $options;
$i=0;
 
if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.': configurazione salvata.</strong></p></div>';
if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.': configurazione reset.</strong></p></div>';
 
?>
<div class="wrap rm_wrap">
<h2>Personalizzazione di <?php echo $themename; ?></h2>
 
<div class="rm_opts">
<form method="post">
<?php foreach ($options as $value) {
switch ( $value['type'] ) {
 
case "open":
?>
 
<?php break;
 
case "close":
?>
 
</div>
</div>
<br />

 
<?php break;
 
case "title":
?>
<p>Per una rapida personalizzazione del tema <b><?php echo $themename;?></b> puoi usare questo menu.</p>

 
<?php break;
 
case 'text':
?>

<div class="rm_input rm_text">
	<label for='<?php echo $value['id']; ?>'><?php echo $value['name']; ?></label>
 	<input name='<?php echo $value['id']; ?>' id='<?php echo $value['id']; ?>' type='<?php echo $value['type']; ?>' value='<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'])  ); } else { echo $value['std']; } ?>' />
 <small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
 
 </div>
<?php
break;
 
case 'textarea':
?>

<div class="rm_input rm_textarea">
	<label for='<?php echo $value['id']; ?>'><?php echo $value['name']; ?></label>
 	<textarea name='<?php echo $value['id']; ?>' type='<?php echo $value['type']; ?>' cols='' rows=''><?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id']) ); } else { echo $value['std']; } ?></textarea>
 <small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
 
 </div>
  
<?php
break;
 
case 'select':
?>

<div class="rm_input rm_select">
	<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
	
<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
<?php foreach ($value['options'] as $option) { ?>
		<option <?php if (get_settings( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?>
</select>

	<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
</div>
<?php
break;
 
case "checkbox":
?>

<div class="rm_input rm_checkbox">
	<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
	
<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />


	<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
 </div>
<?php break; 
case "section":

$i++;

?>

<div class="rm_section">
<div class="rm_title"><h3><img src="<?php bloginfo('template_url')?>/admin/images/trans.png" class="inactive" alt=""><?php echo $value['name']; ?></h3><span class="submit"><input name="save<?php echo $i; ?>" class="button button-primary" type="submit" value="Salva Modifiche" />
</span><div class="clearfix"></div></div>
<div class="rm_options">

 
<?php break;

}
}
?>
 
<input type="hidden" name="action" value="save" />

 </div> 
 

<?php
}
?>
<?php
add_action('admin_init', 'mytheme_add_init');
add_action('admin_menu', 'mytheme_add_admin');
?>