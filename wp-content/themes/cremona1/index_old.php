<?php
/**
 *
 * Description: Default Index template to display loop of blog posts
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>

<?php if (get_option('dueper_blocco1')) : ?>
	<?php get_template_part("includes/streaming"); ?>
<?php endif; ?>

<div class="container">
    <div class="row content">
        <div class="span8">    		
            <?php get_template_part("includes/palinsesto"); ?>
            
            <?php get_template_part("includes/celebrita"); ?>
        
        </div><!-- /.span8 -->
       
        <div class="span4">
        
        	<?php get_sidebar('home'); ?>
        
        </div><!-- /.span4 -->
    </div><!--row-->
</div><!--container-->
<?php get_footer(); ?>