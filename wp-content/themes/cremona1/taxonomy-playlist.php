<?php
/**
 *
 * Description: this is the playlist template
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>

<div class="container">
	<div class="row">
        <div class="span9">
        	<!--elimino la paginazione -->
        	<?php query_posts($query_string."&posts_per_page=12") ?>
            
            <!--eseguo la query di tutti post esistenti-->
      		<?php if (have_posts()) : ?>
            	<div class="row top-margin">
				<?php while (have_posts()) : the_post(); $i=0; $i++; ?>
                     <div <?php post_class('span3');?>>
                     	<?php get_template_part("includes/video-element"); ?>
                    </div><!--span3-->
                    <?php if($i==3): 
					echo "</div><div class'row'>";
					$i=0;
					endif;
					?>
               <?php endwhile; ?>
            	
                <?php wp_pagenavi(); ?>
            
            	</div> <!-- row --> 
			<?php endif; ?>
            <!-- contenuto della pagina --> 		
        </div><!-- span9 -->
	<div class="span3">
        <?php include 'includes/mondopadano.php'; ?>
    <?php get_sidebar('video'); ?>
    </div><!-- span3 -->
    </div><!-- row -->
</div> <!-- container -->


<?php get_footer(); ?>