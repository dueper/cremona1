/********************************************
*
*		Inserisco tutte le mie funzioni custom
*
*********************************************/
 
//
// la funzione inizia a pagina caricata
//
jQuery(document).ready(function(){

// cambio opacity all'hover sulle immagini con classe .img-polaroid
	jQuery('img.img-polaroid').each(function() {
		jQuery(this).hover(
			function() {
				jQuery(this).stop().animate({ opacity: 0.3 }, 200);
			},
	   function() {
		   jQuery(this).stop().animate({ opacity: 1 }, 200);
	   })
	});
// fine: cambio opacity all'hover sulle immagini con classe .img-polaroid

	
});

//
// Questa funzione serve per avere il dropdown sull'hover
//

jQuery(document).ready(function() {
      jQuery('.dropdown-toggle').dropdownHover();
});



//spingo il footer sul fondo della pagina
jQuery(document).ready(function() {
    var height_diff = jQuery(window).height() - jQuery('body').height();
    if ( height_diff > 0 ) {
        jQuery('footer').css( 'margin-top', height_diff - 10  );
    }
});


//
// button scroll to top opacity
//

jQuery(document).ready(function(){ 
 
	jQuery(window).scroll(function(){
		if (jQuery(this).scrollTop() > 100) {
			jQuery('.scrollup').fadeIn();
		} else {
			jQuery('.scrollup').fadeOut();
		}
	}); 
	
});
//
// setup button scroll to top
//
jQuery(document).ready(function(){
      jQuery('a[href=#head]').click(function(){
        jQuery('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });

});