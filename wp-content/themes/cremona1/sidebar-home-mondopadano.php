<?php
/**
 * The Sidebar of homepage
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */
?>

<!--/*
  *
  * Revive Adserver Javascript Tag
  * - Generated with Revive Adserver v3.1.0
  *
  */-->

<!--/*
  * The backup image section of this tag has been generated for use on a
  * non-SSL page. If this tag is to be placed on an SSL page, change the
  *   'http://adserver.dueperdev.it/revive-adserver/www/delivery/...'
  * to
  *   'https://adserver.dueperdev.it/revive-adserver/www/delivery/...'
  *
  * This noscript section of this tag only shows image banners. There
  * is no width or height in these banners, so if you want these tags to
  * allocate space for the ad before it shows, you will need to add this
  * information to the <img> tag.
  *
  * If you do not want to deal with the intricities of the noscript
  * section, delete the tag (from <noscript>... to </noscript>). On
  * average, the noscript tag is called from less than 1% of internet
  * users.
  */-->
<div class="mondopadano-container">
<!--/*
  *
  * Revive Adserver Asynchronous JS Tag
  * - Generated with Revive Adserver v3.2.1
  * - Revive Adserver
  *
  */-->

<ins data-revive-zoneid="13" data-revive-target="_blank" data-revive-ct0="{clickurl_enc}" data-revive-block="1" data-revive-id="be6b165c370de5c1fbae0e48bdec4aa0"></ins>
<script async src="//adv.cremonaoggi.it/www/delivery/asyncjs.php"></script>
</div>

<div class="sidebar-nav">
<h4 class="titoletto bg-blue">Facebook</h4>

<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fcremona1tv&amp;width=292&amp;height=558&amp;show_faces=true&amp;colorscheme=light&amp;stream=true&amp;show_border=false&amp;header=false&amp;appId=294820310648819" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:558px;" allowTransparency="true"></iframe>

</div><!--.sidebar-nav -->
