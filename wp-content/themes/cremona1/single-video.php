<?php
/**
 * The template for displaying all posts.
 *
 * Default Post Template
 *
 * Page template with a fixed 940px container and right sidebar layout
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>
<?php if ( have_posts() ) : the_post(); ?>

<?php $videourl = get_field('url');
    
    if ($videourl != '') :
    
    $link= $videourl;
    
    $testo=explode("watch?v=", $link);
    
    $tot=strlen($testo[1]);
    $i=0;
    $lettera=$testo[1][$i];
    $stringa="";
    while (($i<$tot)AND($lettera!="&")){
    $i++;
    $stringa.=$lettera;
    $lettera=$testo[1][$i];
    }
    ?>
    <img src="http://i.ytimg.com/vi/<?php echo $stringa; ?>/hqdefault.jpg" style="display:none;" />	
    <?php endif; ?>

<div class="container top-margin">     
    <div class="row content">
        <div class="span9">
            <?php
			$post_ID = $post->ID;
            $url = get_field('url');
			$embed_code = wp_oembed_get( $url, $args );   ?>
            <div class="video-container">
				<?php echo $embed_code; ?>
        	</div>
            <header class="post-title">
                <h2><?php the_title();?></h2>
            </header>
            <hr />
            <?php
			$term_arg = array('parent' => 0);
			
            $terms = get_the_terms($post->ID,"playlist");
            $count = count($terms);
             if ( $count > 0 ) :
                 foreach ( $terms as $term ) {
                   $termslug = $term->slug;
				   $termname = $term->name;
                 }
            
			
				$args = array(
					'post_type'=>'video',
					'playlist'=> $termslug,
					'posts_per_page'=>6,
					'post__not_in' => array($post_ID)
					
				);
				query_posts($args);
				
				if (have_posts()) : ?>
                <h3>Altri video di <?php echo $termname; ?></h3>
					<div class="row">
					<?php while ( have_posts() ) : the_post(); ?>
						<div <?php post_class('span3'); ?>>
							<?php get_template_part("includes/video-element"); ?>
						</div><!--span3-->
						<?php if($i==3): 
							echo "</div><div class'row'>";
							$i=0;
						endif;
						?>
					<?php endwhile; ?>
					</div><!--row-->
				<?php endif; ?>
			<?php endif; //if count exist?>
        </div><!--span9-->
        
        <div class="span3">
              <?php get_sidebar('video'); ?>
        </div><!--span3-->
    </div><!-- .row content -->
</div><!--container-->
<?php endif; // end of the loop. ?>
<?php get_footer(); ?>