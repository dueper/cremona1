<?php
/**
 * Template Name: Blog Page
 * Description: Page template to display blog posts
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.infinitescroll.js" type="text/javascript" charset="utf-8"></script>    

<script type="text/javascript">

jQuery(document).ready(

function($){

  jQuery('#content').infinitescroll({
    navSelector  : "div.load_more_text",  
                   // selector for the paged navigation (it will be hidden)
    nextSelector : "div.load_more_text a:first",
                   // selector for the NEXT link (to page 2)
    itemSelector : "#content_inside .post_box"
                   // selector for all items you'll retrieve
  });  

}  

);

</script>


<?php if ( have_posts() ) : the_post(); ?>
<div class="container">
  <div class="row">
  <div class="span12">
      <?php if (function_exists('bootstrapwp_breadcrumbs')) bootstrapwp_breadcrumbs(); ?>
  </div>
  </div><!--/.row -->
</div><!--/.container -->
<div class="container">
 <!-- Masthead
 ================================================== -->
    <header class="subhead">
      <h1><?php the_title();?></h1>
    </header>
    
    <div class="row content">
        <div class="span12" id="content">
            <div id="content_inside">
                <?php
                              // Blog post query
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    query_posts( array( 'post_type' => 'post', 'paged'=>$paged, 'posts_per_page'=>4) );
                    
                    if (have_posts()) : while ( have_posts() ) : the_post(); ?>
                        <div <?php post_class('post_box'); ?>>
                          <a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><h3><?php the_title();?></h3></a>
                          
                          <div class="row">
                            <div class="span4">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                  <?php the_post_thumbnail('large4-squared'); ?>
                                </a>
                    
                            </div><!-- /.span2 -->
                            <div class="span6">
                            <?php the_excerpt();?>
                            </div><!-- /.span6 -->
                         </div><!-- /.row -->
                         <hr />
                       </div><!-- /.post_class -->
                 <?php endwhile; endif; ?>
            </div><!-- /.content-inside -->
        
             <div class="load_more_cont">
                <div class="load_more_text">
                    <?php
                    ob_start();
                    next_posts_link('<img src="' . get_bloginfo('stylesheet_directory') . '/img/loading-button.png" />');
                    $buffer = ob_get_contents();
                    ob_end_clean();
                    if(!empty($buffer))
                    echo $buffer;
                    ?>
                </div>
            </div><!--//load_more_cont-->
            
        </div><!-- /.span12 -->
    
    </div><!--row-->
</div><!--container-->
<?php endif; ?>
<?php get_footer(); ?>