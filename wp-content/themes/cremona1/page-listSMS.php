<?php
/**
 * Template Name: Elenco SMS
 * Description: Page template to display blog posts
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header('sms'); ?>

<?php if(is_user_logged_in()): ?>
       
        <?php if ( have_posts() ) : the_post(); ?>
        <div class="container">
            <?php if(current_user_can('edit_post')): ?>
                        <div id="content_inside">
                            <?php // SMS post query
                            $sms_args = array( 'post_type' => 'sms', 'posts_per_page' => 500);
                            $sms_posts = new WP_Query($sms_args);

                            if ($sms_posts->have_posts()) : 
                                while( $sms_posts->have_posts()) :
                                  $sms_posts->the_post();
                                  if(get_field('da_leggere')):
                                      $marked = 'marked';
                                  else:
                                      $marked = '';
                                  endif; ?>
                                  <div <?php post_class('post_box '.$marked); ?>>
                                      <div class="row-fluid">
                                          <div class="span9">
                                              <span class="number"><?php the_title();?></span>
                                              <div class="lead"><?php the_content();?></div>
                                          </div><!--span10-->
                                          <div class="span3 text-right">
                                              <button data-post_id="<?php the_ID(); ?>" class="deleteSMS"><span class="entypo">&#59177;</span></button>
                                              <?php if(get_field('da_leggere')): ?>
                                                  <button data-post_id="<?php the_ID(); ?>" class="lettoSMS"><span class="entypo">&#9733;</span></button>
                                              <?php else: ?>
                                                  <button data-post_id="<?php the_ID(); ?>" class="lettoSMS letto"><span class="entypo">&#9734;</span></button>
                                              <?php endif; ?>
                                              <date>
                                                  <span class="hour"><?php echo get_the_date('H:i') ?></span><span class="data"><?php echo get_the_date('d F Y'); ?></span>
                                              </date>
                                          </div><!--span3-->
                                      </div><!--row-->
                                 </div><!-- /.post_class -->
                              <?php endwhile;
                          endif; ?>
                        </div><!-- /.content-inside -->


                <script>
                    jQuery(document).ready(function() {

                        jQuery(".lettoSMS").click( function() {
                                var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                                var post_id = jQuery(this).attr("data-post_id");

                                var data = {
                                    'action': 'letto',
                                    'post_id': post_id
                                };

                                jQuery.post(ajaxurl, data, function(response) {
                                    //alert('Risposta: ' + response);
                                    location.reload();
                                });
                        });

                        jQuery(".deleteSMS").click( function() {
                                var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                                var post_id = jQuery(this).attr("data-post_id");

                                var data = {
                                    'action': 'deletesms',
                                    'post_id': post_id
                                };
                                var conf = confirm("Sei sicuro di cancellare questo SMS");
                                if (conf == true) {
                                    jQuery.post(
                                        ajaxurl,
                                        data,
                                        function(response) {
                                            //alert('Risposta: ' + response);
                                            location.reload();
                                        }
                                    );
                                }
                        });
                    });
                </script>

            <?php endif; ?>
            
            <div class="text-center" style="margin:30px 0;">
                <a href="http://www.cremona1.it/wp-admin/edit.php?post_type=sms" class="btn btn-primary">Vai all'amministrazione</a>
            </div>
            
        </div><!--container-->
        <?php endif; ?>
<?php else : //is_user_logged_in() ?>
    <div class="container text-center">
        <?php wp_login_form( $args ); ?> 
    </div><!--container-->
<?php endif; ?>

<?php get_footer(); ?>

