<?php
/**
 * Template Name: Full-width Page
 * Description: A full-width template with no sidebar
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

<div class="container">
    <div class="row content">
       <div class="span12">
        
            <header>
                <h1><?php the_title();?></h1>
            </header>
            
            <?php the_content(); ?>
        
        </div><!-- /.span8 -->
       
        
    </div><!-- .row content -->
</div><!--container-->
		
 <?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>