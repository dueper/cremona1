<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */
?>

<div class="sidebar-nav">

<!-- Includo Widget -->
<?php if ( function_exists('dynamic_sidebar')) dynamic_sidebar("sidebar-page"); ?>

<!-- Includo Twitter -->
<?php get_template_part("includes/twitterfeed"); ?>

<!-- Includo Facebook -->
<?php get_template_part("includes/facebook"); ?>

</div><!--/.well .sidebar-nav -->