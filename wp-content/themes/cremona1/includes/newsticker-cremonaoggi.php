<?php
$url = "http://feeds.feedburner.com/Cremonaoggi";
$rss = simplexml_load_file($url);
$i=0;
if($rss)
{ ?>
<div class="container">
    <div class="raw">
    
            <ul id="js-news">
            
            <?php 
            $items = $rss->channel->item;
            foreach($items as $item){
                if($i<6){
				$title = $item->title;
				$link = $item->link;
				$published_on = $item->pubDate;
				$description = $item->description; ?>
            
            <li  class="news-item"><a href="<?php echo $link ?>" target="_blank" class="link_inv"><?php echo $title; ?></a></li>
                
            <?php
                $i++;} }
            ?>
            </ul>
              
    </div><!--raw-->
</div><!--container-->
<?php 
}
?>

<script type="text/javascript">
    jQuery(function () {
        jQuery('#js-news').ticker();
    });
</script>