<h4 class="titoletto bg-blue">Palinsesto</h4>
<div class="row-fluid home-loop">
	<div class="span12">
		<?php
            query_posts( array( 'post_type' => 'palinsesto', 'order' => 'ASC', 'posts_per_page'=>7) );
        if (have_posts()): ?>
            <ul class="nav nav-tabs" id="palinsesto-tab">
            <?php while (have_posts()) : the_post(); ?>
            
                <?php $id = get_the_id(); ?>
                
                  <li><a href="#panel-<?php echo $id; ?>" data-toggle="tab"><?php the_field('giorno'); ?></a></li>
                  
            <?php endwhile; ?>
            </ul><!--nav-tabs-->
            
        <?php endif;
        wp_reset_query();
		
        query_posts( array( 'post_type' => 'palinsesto') );
        if (have_posts()) : ?>
                    <div class="tab-content">
                    <?php while (have_posts()) : the_post(); ?>
                        
                        <?php $id = get_the_id(); ?>
                        
                            <div class="tab-pane" id="panel-<?php echo $id; ?>">
                                
                                <div class="accordion" id="accordion-<?php echo $id; ?>">
                                    <!--inner-->
                                      <div class="accordion-group">
                                        <div class="accordion-heading">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-<?php echo $id; ?>" href="#collapseMattino-<?php echo $id; ?>">
                                            Mattino del <?php the_title(); ?>
                                          </a>
                                        </div><!--accordion-heading-->
                                        <div id="collapseMattino-<?php echo $id; ?>" class="accordion-body collapse in">
                                          <div class="accordion-inner">
                                            
                                            <?php if(get_field('mattino')): ?>
                                                <?php while(has_sub_field('mattino')): ?>
                                                	<?php $posts = get_sub_field('programma'); foreach( $posts as $post): setup_postdata($post);?>
                                                        <div class="row-fluid <?php the_sub_field('bg'); ?>">
                                                              <div class="span12">
                                                              
                                                                <h4><span class="label"><?php the_sub_field('ora_inizio'); ?></span> <?php echo the_title(); ?></h4>
                                                                <?php if (get_sub_field('descrizione_programma')): 
																the_sub_field('descrizione_programma');
																else: 
																the_content();
																endif;?>
                                                              </div><!--span12-->
                                                        </div><!--row-->
                                                        <hr class="small" />
                                                    <?php endforeach; wp_reset_postdata();?>
                                                <?php wp_reset_postdata(); endwhile; ?>
                                            <?php endif; ?> 
                                          
                                          </div><!--accordion-inner-->
                                        </div><!--accordion-body-->
                                      </div><!--accordion-group-->
                                      
                                      <div class="accordion-group">
                                        <div class="accordion-heading">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-<?php echo $id; ?>" href="#collapsePomeriggio-<?php echo $id; ?>">
                                            Pomeriggio del <?php the_title(); ?>
                                          </a>
                                        </div><!--accordion-heading-->
                                        <div id="collapsePomeriggio-<?php echo $id; ?>" class="accordion-body collapse">
                                          <div class="accordion-inner">
                                                       
                                           <?php if(get_field('pomeriggio')): ?>
                                                <?php while(has_sub_field('pomeriggio')): ?>
                                                	<?php $posts = get_sub_field('programma'); foreach( $posts as $post): setup_postdata($post);?>
                                                        <div class="row-fluid <?php the_sub_field('bg'); ?>">
                                                              <div class="span12">
                                                              
                                                                <h4><span class="label"><?php the_sub_field('ora_inizio'); ?></span> <?php echo the_title(); ?></h4>
                                                                <?php if (get_sub_field('descrizione_programma')): 
																the_sub_field('descrizione_programma');
																else: 
																the_content();
																endif;?>
                                                              </div><!--span12-->
                                                        </div><!--row-->
                                                        <hr class="small" />
                                                    <?php endforeach; wp_reset_postdata();?>
                                                <?php wp_reset_postdata(); endwhile; ?>
                                            <?php endif; ?> 
                                         
                                          </div><!--accordion-inner-->
                                        </div><!--accordion-body-->
                                      </div><!--accordion-group-->
                                      
                                      <div class="accordion-group">
                                        <div class="accordion-heading">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-<?php echo $id; ?>" href="#collapseSera-<?php echo $id; ?>">
                                            Sera del <?php the_title(); ?>
                                          </a>
                                        </div><!--accordion-heading-->
                                        <div id="collapseSera-<?php echo $id; ?>" class="accordion-body collapse">
                                          <div class="accordion-inner">
                                          
                                            <?php if(get_field('sera')): ?>
                                                <?php while(has_sub_field('sera')): ?>
                                                	<?php $posts = get_sub_field('programma'); foreach( $posts as $post): setup_postdata($post);?>
                                                        <div class="row-fluid <?php the_sub_field('bg'); ?>">
                                                              <div class="span12">
                                                              
                                                                <h4><span class="label"><?php the_sub_field('ora_inizio'); ?></span> <?php echo the_title(); ?></h4>
                                                                <?php if (get_sub_field('descrizione_programma')): 
																the_sub_field('descrizione_programma');
																else: 
																the_content();
																endif;?>
                                                              </div><!--span12-->
                                                        </div><!--row-->
                                                        <hr class="small" />
                                                    <?php endforeach; wp_reset_postdata();?>
                                                <?php wp_reset_postdata(); endwhile; ?>
                                            <?php endif; ?> 
                                            
                                          </div><!--accordion-inner-->
                                        </div><!--accordion-body-->
                                      </div><!--accordion-group-->
                                      
                                      <div class="accordion-group">
                                        <div class="accordion-heading">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-<?php echo $id; ?>" href="#collapseNotte-<?php echo $id; ?>">
                                            Notte del <?php the_title(); ?>
                                          </a>
                                        </div><!--accordion-heading-->
                                        <div id="collapseNotte-<?php echo $id; ?>" class="accordion-body collapse">
                                          <div class="accordion-inner">
                                            
                                            <?php if(get_field('notte')): ?>
                                                <?php while(has_sub_field('notte')): ?>
                                                	<?php $posts = get_sub_field('programma'); foreach( $posts as $post): setup_postdata($post);?>
                                                        <div class="row-fluid <?php the_sub_field('bg'); ?>">
                                                              <div class="span12">
                                                              
                                                                <h4><span class="label"><?php the_sub_field('ora_inizio'); ?></span> <?php echo the_title(); ?></h4>
                                                                <?php if (get_sub_field('descrizione_programma')): 
																the_sub_field('descrizione_programma');
																else: 
																the_content();
																endif;?>
                                                              </div><!--span12-->
                                                        </div><!--row-->
                                                        <hr class="small" />
                                                    <?php endforeach; wp_reset_postdata();?>
                                                <?php wp_reset_postdata(); endwhile; ?>
                                            <?php endif; ?> 
                                            
                                          </div><!--accordion-inner-->
                                        </div><!--accordion-body-->
                                      </div><!--accordion-group-->
                                      
                                </div><!--accordion-->
                                
                            </div><!--tab-pane-->
                         
                        
                    <?php endwhile; ?>
                    </div><!--tab-content-->
		   <script>
              jQuery(function () {
                jQuery('#palinsesto-tab a:first').tab('show');
              })
            </script>
        
        <?php endif; ?>
	</div><!--span12-->
</div><!-- row-->