<?php
/**
 *
 * Default Page Header
 *
 * @package WP-Bootstrap
 * @subpackage Default_Theme
 * @since WP-Bootstrap 0.1
 *
 * Last Revised: August 15, 2012
 */ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
   <title><?php
  /*
   * Print the <title> tag based on what is being viewed.
   */
  global $page, $paged;

  wp_title( '|', true, 'right' );

  // Add the blog name.
  bloginfo( 'name' );

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', 'bootstrapwp' ), max( $paged, $page ) );

  ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <!-- senza zoom sui mobile -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!-- con zoom sui mobile
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    -->
    
    
    
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- Le fav and touch icons -->

<link rel="shortcut icon" href="http://www.cremona1.it/favicon.ico">

<?php if (get_option('dueper_iphone_144')) : ?>
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_option('dueper_iphone_144'); ?>">
<?php endif; ?>
<?php if (get_option('dueper_iphone_114')) : ?>
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_option('dueper_iphone_114'); ?>">
<?php endif; ?>
<?php if (get_option('dueper_iphone_72')) : ?>
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_option('dueper_iphone_72'); ?>">
<?php endif; ?>
<?php if (get_option('dueper_iphone_57')) : ?>
<link rel="apple-touch-icon-precomposed" href="<?php echo get_option('dueper_iphone_57'); ?>">
<?php endif; ?>

  <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    <?php wp_head(); ?>
    
<?php if (get_option('dueper_animate')) : ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/animate/animate.min.css" type="text/css" media="screen" charset="utf-8">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/css/animate/animate.js"></script>
<?php endif; ?>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41333785-1', 'cremona1.it');
  ga('send', 'pageview');

</script>

  </head>
  <body <?php body_class(); ?>  data-spy="scroll" data-target=".bs-docs-sidebar" data-offset="10">
  
<div class="bg-smoke logo">
  
  <div class="container text-center">
	<?php if (get_option('dueper_logo')) : ?>
    <a class="brand" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_option('dueper_logo'); ?>" /></a>
    <?php else : ?>
    <a class="brand" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
    <?php endif; ?>
  </div><!--container--> 
</div><!--bg-smoke-->
   
<div class="row bg-lightblue menu-contenitore">
    <div class="container nav-margin">
        <div role="navigation" class="navbar">
            <div class="navbar-inner">
           
            </div><!-- .navbar-inner -->
        </div><!-- .navbar -->
    </div><!-- container -->
</div><!--bg-lightblue-->