<?php
/**
 * Default Footer
 *
 * @package WP-Bootstrap
 * @subpackage Default_Theme
 * @since WP-Bootstrap 0.1
 *
 * Last Revised: July 16, 2012
 */
?>
    <!-- End Template Content -->
<footer class="bg-darkblue">
<div class="container">        
        <a href="#head" class="scrollup">TOP</a>
        <p>&copy; <?php bloginfo('name'); ?> <?php the_time('Y') ?> | <?php echo get_option('dueper_footer_text'); ?></p>
        
          <?php if ( function_exists('dynamic_sidebar')) dynamic_sidebar("footer-content"); ?>
    </div> <!-- /container -->
</footer>
<?php wp_footer(); ?>



<?php if (get_option('dueper_debug')) : ?>

<script type="text/javascript">
    jQuery(window).load(function(){
        jQuery('#debug').modal('show');
    });
</script>

<div id="debug" class="modal show fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Debug Mode Attiva</h3>
  </div>
  <div class="modal-body">
    <p>
    <i class="icon-refresh"></i> Numero di queries: <span class="lead"><?php echo get_num_queries(); ?> query</span><br/>
    <i class="icon-time"></i> Pagina caricata in: <span class="lead"><?php timer_stop(1); ?> secondi</span><br/>
    <i class="icon-download-alt"></i> Peak Memory Used: <span class="lead"><?php echo round(memory_get_peak_usage() / 1024 / 1024, 3);?> Mb</span><br/>
    </p>
  </div>
  <div class="modal-footer">
    <a href="#debug" class="btn btn-primary" data-toggle="modal">Close</a>
  </div>
</div>

<?php endif; ?>


  </body>
</html>