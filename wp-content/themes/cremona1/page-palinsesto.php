<?php
/**
 * Template Name: Palinsesto Page
 * Description: A full-width template with no sidebar
 *
 * @package WordPress
 * @subpackage WP-Bootstrap
 * @since WP-Bootstrap 0.1
 */
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

<div class="container">
    <div class="row content">
       <div class="span8">
            <h1><?php the_title();?></h1>
            
            <?php get_template_part('includes/palinsesto'); ?>
            
            <?php the_content(); ?>
        
        </div><!-- /.span8 -->
       
        <div class="span4">
        
        	<?php get_sidebar(); ?>
        
        </div><!--span4-->
    </div><!-- .row content -->
</div><!--container-->
		
 <?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>